#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <windows.h> 

int paralelSifrovanie(char *inputFile, char *outputFile) {
	double omp_time_start, omp_time_end;
	FILE *file, *out;

	int shift = 10;
	printf("Press any key to start encoding input file %s...", inputFile);
	getchar();
	file = fopen(inputFile, "r");

	fseek(file, 0, SEEK_END);
	long fsize = ftell(file);
	fseek(file, 0, SEEK_SET);

	char *string = malloc(fsize + 1);
	fread(string, fsize, 1, file);

	out = fopen(outputFile, "w+");
	
	omp_time_start = omp_get_wtime();

	int i = 0;
	#pragma omp parallel for schedule(static, 30000)
	for (i = 0; i < fsize; i++) {
		char ch = *(string + i);
		int number = (int)(ch + shift);
		*(string + i) = (char)number;
	}

	omp_time_end = omp_get_wtime();
	fclose(file);
	fclose(out);

	return (omp_time_end - omp_time_start) / (CLOCKS_PER_SEC / 1000);
}

