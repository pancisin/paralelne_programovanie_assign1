#include <stdio.h>

int main(int argc, char *argv[]) {
	if (argc < 4) {
		printf("File I/O must be defined within command line args! eg. -s input.txt output.txt . Press any key to exit...");
		getchar();
		return 1;
	}

	char *input_file = argv[2];
	char *output_file = argv[3];

	int result = 0;

	if (strcmp(argv[1], "-s") == 0)
		result = serialSifrovanie(input_file, output_file);
	else
		result = paralelSifrovanie(input_file, output_file);

	printf("Success! CPU time: %f s \n", result);

	getchar();
	return 0;
}