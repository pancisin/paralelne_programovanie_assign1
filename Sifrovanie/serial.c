#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>

int serialSifrovanie(char *inputFile, char *outputFile) {
    clock_t stopwatchCPUStart, stopwatchCPUEnd;
    int stopwatchStart, stopwatchEnd;
    FILE *file, *out;

    int shift = 10;
	printf("Press any key to start encoding input file %s...", inputFile);
	getchar();
    file = fopen(inputFile, "r");

    fseek(file, 0, SEEK_END);
    long fsize = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *string = malloc(fsize + 1);
    fread(string, fsize, 1, file);

    out = fopen(outputFile, "w+");
    
	stopwatchCPUStart = clock();

    int i = 0;
    for (i = 0; i < fsize; i++) {
        char ch = *(string + i);
        int number = (int) (ch + shift);
        *(string + i) = (char) number;
    }

    stopwatchCPUEnd = clock();
    
	fclose(file);
    fclose(out);

	return (stopwatchCPUEnd - stopwatchCPUStart) / (CLOCKS_PER_SEC / 1000);
}

