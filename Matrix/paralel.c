#include <omp.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

void paralelMultiplication(int rows1, int cols1, int rows2, int cols2) {
	int **m1, **m2, **result;
	double omp_time_start, omp_time_end;

	srand(time(NULL));

	printf("\nDefined first matrix size %d X %d (rows X cols)\n\n", rows1, cols1);
	printf("\nDefined second matrix size %d X %d (rows X cols)\n\n", rows2, cols2);

	m1 = allocate_metrix_memory(rows1, cols1);
	m2 = allocate_metrix_memory(rows2, cols2);
	result = allocate_metrix_memory(rows1, rows1);
	printf("Memory for matrixs allocated.\n\n");

	fill_with_random(m1, rows1, cols1);
	fill_with_random(m2, rows2, cols2);
	printf("Matrixs filled with random integers.\n\n");

	omp_time_start = omp_get_wtime();

	//#pragma omp parallel for schedule(static, 500)
	int r;
#pragma omp parallel for schedule (dynamic)
	for (r = 0; r < rows1; r++) {
		int sum = 0;
		int c;
		for (c = 0; c < cols2; c++) {
 			result[r][c] = m1[r][c] * m2[c][r];
		}
	}

	omp_time_end = omp_get_wtime();
	printf("Matrixs multiplied.\n\n");
	printf("CPU time: %f s \n", (omp_time_end - omp_time_start) / (CLOCKS_PER_SEC / 1000));
}