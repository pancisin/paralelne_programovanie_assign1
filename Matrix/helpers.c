int** allocate_metrix_memory(int rows, int columns) {
	int** matrix = (int**)malloc(rows * sizeof(int*));
	int r;
	for (r = 0; r < rows; r++) {
		matrix[r] = (int*)malloc(columns * sizeof(int));
	}

	return matrix;
}

void fill_with_random(int **matrix, int rows, int columns) {
	int r, c;
	for (r = 0; r < rows; r++) {
		for (c = 0; c < columns; c++) {
			matrix[r][c] = rand() % 100;
		}
	}
}