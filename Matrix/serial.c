#include <omp.h>
#include <time.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void serialMultiplication(int rows1, int cols1, int rows2, int cols2) {
	int **m1, **m2, **result;
	clock_t stopwatchCPUStart, stopwatchCPUEnd;
	
	srand(time(NULL));

	printf("\nDefined first matrix size %d X %d (rows X cols)\n\n", rows1, cols1);
	printf("\nDefined second matrix size %d X %d (rows X cols)\n\n", rows2, cols2);

	m1 = allocate_metrix_memory(rows1, cols1);
	m2 = allocate_metrix_memory(rows2, cols2);
	result = allocate_metrix_memory(rows1, rows1);
	printf("Memory for matrixs allocated.\n\n");

	fill_with_random(m1, rows1, cols1);
	fill_with_random(m2, rows2, cols2);
	printf("Matrixs filled with random integers.\n\n");

	stopwatchCPUStart = clock();

	int r;
	for (r = 0; r < rows1; r++) {
		int sum = 0;
		int c;
		for (c = 0; c < cols2; c++) {
			result[r][c] = m1[r][c] * m2[c][r];
		}
	}

	stopwatchCPUEnd = clock();

	printf("Matrixs multiplied.\n\n");

	printf("CPU time: %d ms \n", (stopwatchCPUEnd - stopwatchCPUStart) / (CLOCKS_PER_SEC / 1000));
}