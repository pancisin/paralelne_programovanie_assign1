#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

int main(int argc, char *argv[]) {
	int rows1, cols1, rows2, cols2;

	printf("Enter the number of rows and columns of first matrix\n");
	scanf("%d %d", &rows1, &cols1);
	printf("Enter the number of columns of second matrix (number of rows is %d)\n", cols1);
	scanf("%d", &cols2);

	if (argc > 1 && strcmp(argv[1], "-s") == 0) {
		serialMultiplication(rows1, cols1, cols1, cols2);
	}
	else {
		paralelMultiplication(rows1, cols1, cols1, cols2);
	}

	getchar();
	getchar();

	return 0;
}