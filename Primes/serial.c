#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <Windows.h>

int serialPrimes(long number) {
	clock_t stopwatchCPUStart, stopwatchCPUEnd;
	int stopwatchStart, stopwatchEnd;

	printf("Hit enter for finding primes from %ld numbers...\n", number);
	getchar();

	stopwatchCPUStart = clock();

	long numberOfPrimes = 0;
	long i;

	for (i = 1; i < number; i++) {
		long j;
		bool isPrime = true;
		long sqrtI = (long)sqrt((double)i);
		for (j = 2; j < sqrtI; j++) {
			if (i % j == 0) {
				isPrime = false;
				break;
			}
		}
		if (isPrime) {
			numberOfPrimes++;
		}
	}


	stopwatchCPUEnd = clock();

	printf("Work done. Found: %ld primes.\n", numberOfPrimes);
	printf("CPU time: %d ms \n", (stopwatchCPUEnd - stopwatchCPUStart) / (CLOCKS_PER_SEC / 1000));

	return (EXIT_SUCCESS);
}

