#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <omp.h>
#include <math.h>
#include <Windows.h>

int paralelPrimes(long number) {
	double omp_time_start, omp_time_end;

    printf("Hit enter for finding primes from %ld numbers...\n", number);
    getchar();

	omp_time_start = omp_get_wtime();

    long numberOfPrimes = 0;
    long i;
    omp_lock_t writelock;

    omp_init_lock(&writelock);

    #pragma omp parallel for schedule(static, 30000)
    for (i = 1; i < number; i++) {
        long j;
        bool isPrime = true;
        long sqrtI = (long) sqrt((double) i);
        for (j = 2; j < sqrtI; j++) {
            if (i % j == 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            omp_set_lock(&writelock);
            numberOfPrimes++;
            omp_unset_lock(&writelock);
        }
    }


    omp_destroy_lock(&writelock);

	omp_time_end = omp_get_wtime();

    printf("Work done. Found: %ld primes.\n", numberOfPrimes);

    printf("CPU time: %f s \n", (omp_time_end - omp_time_start) / (CLOCKS_PER_SEC / 1000));
    return (EXIT_SUCCESS);
}

