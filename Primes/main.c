#include <stdio.h>

int main(int argc, char *argv[]) {
	long numbers = 250000;

	if (argc == 3)
		numbers = strtol(argv[2], (char **)NULL, 10);

	if (argc >= 2 && strcmp(argv[1], "-s") == 0)
		serialPrimes(numbers);
	else
		paralelPrimes(numbers);

	getchar();
	return 0;
}